% Course proposal: Programs, proofs and types
% Piyush P Kurur

# Introduction.

A software bug in your fancy mobile app is a mere annoyance. However,
in critical systems like railway signalling and flight control, it can
be fatal. Engineering such critical software demands constructing
programs which are guaranteed to meet the correctness
specifications. How does one build such software? Register for this
course and get an answer.

The main theme of this course is _certified programs_, i.e. programs
that come with a proof that it meets the desired specifications. The
purported proofs are _formal_ in the sense that they are machine
checked for correctness. In this course, we study how to use the proof
assistant [Coq] to build such certified programs.

Although this course is about certified programs, the underlying
theory is quite general. Proof assistants like [Coq] are powerful
enough to formalise and prove virtually any mathematical theorems ---
for a recent success see the complete proof of
[Feit-Thompson theorem in Coq][ftcoq]). Recent developments in this
area show deep connections to logic, foundations of mathematics and
homotopy theory (topology). Thus this course could be of interest to
mathematicians and logicians as well, although our focus would mostly
be on certified programming.

## Contents

The following topics will be covered in this course

1. [Coq] the proof assistant,

2. Functional and dependent types programming in [Coq],

3. Proof tactics and proof automation.


If time permits we will look at some advanced topics in type theory.

## Who should take this course?

The focus of this course is certified programming. Nevertheless, it
should also be of interest to mathematicians and logicians who want to
know how a proof assistant like [Coq] work and what they are the good
for.

## Pre-requisites.

An introduction to functional programming is desirable although I have
not put it as an official pre-requisite. If you have a decent
background in a functional programming language like [Haskell] or [ML]
or are confident that the necessary background can be picked up during
the course, then you are welcome to join this course.

This course should be considered as a fairly advanced course and hence
plenty of mathematical maturity is expected. You should also be
comfortable installing [coq] and associated software.  Latest
GNU/Linux distribution comes with pre-packaged versions of coq that
should be sufficient for our purposes.

## References

I plan to follow the book
[Certified programming using dependent types][cpdt], by
[Adam Chlipala][adam] for this course.

All course related material (including the
[latest version of this course proposal][proposal]) is available at
<https://bitbucket.org/piyush-kurur/ppt>.


[cpdt]: <http://adam.chlipala.net/cpdt/> "Certified programming using dependent types"
[adam]: <http://adam.chlipala.net/> "Adam Chlipala"
[coq]: <https://coq.inria.fr/> "The coq proof assistant"
[haskell]: <https://www.haskell.org/> "Haskell programming language"
[ml]: <http://sml-family.org/> "Standard ML Family"
[ftcoq]: <http://www.msr-inria.fr/news/feit-thomson-proved-in-coq/>
[proposal]: <https://bitbucket.org/piyush-kurur/ppt/src/master/proposal/proposal.md>
