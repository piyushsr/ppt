Inductive LE : nat -> nat -> Prop :=
| zLEn    {n   : nat} : LE 0 n
| snLEsm  {m n : nat} : LE n m -> LE (S n) (S m)
.

Hint Constructors LE.

Notation "x <= y" := (LE x y).
Notation "x < y"  := (LE (S x) y).
Notation "x >= y" := (LE y x).
Notation "x > y" :=  (LE (S y) x).

Ltac crush_LE :=
  match goal with
    | [ |- 0 <= _ -> _        ]  => intro
    | [ |- 0 > _  -> _        ]  => assert False
  end.


Ltac crush_existential :=
  match goal with
    | [ n : nat |- exists k, 0 + k = ?n ] => exists n; trivial
    | [ H : exists _ : nat, ?n + _ = ?m  |- exists _:nat,S ?n + _ = S ?m ]
      => destruct H as [k HP]; exists k; simpl; auto
    | _ => idtac
  end.


Ltac crush_function_goal :=
  let H := fresh "H" in
  match goal with
    | [ |- _ -> False       ]   => intro H; inversion H
    | [ |- _ -> _           ]   => intro H; induction H
  end.


Ltac intro_a tac :=
  let H := fresh "H" in intro H ;  tac H.


Ltac intro_and_induction :=
  let H := fresh "H" in intro H ;  induction H.


Ltac crush :=
  repeat match goal with
           | [ _ : False |- _      ]   => contradiction
           | [ H : ?T |- ?T        ]   => exact H
           | [ |-  _ <= _          ]   => crush_LE
           | [ |- exists _,_       ]   => crush_existential
           | [ |- _ -> _           ]   => crush_function_goal
           | _                         => eauto
          (* | _                         => trivial; autorewrite with core*)
         end.

Lemma refLE : forall (x : nat),  x <= x.
Proof.
  crush.
Qed.


Lemma positive_diplacement (x : nat)(y : nat): x <= y  -> exists k : nat, x + k = y.
Proof.
  crush.
Qed.

Lemma zgtxfalse : forall {x : nat},  0 > x -> False.
Proof.
  crush.
Qed.


Lemma nlen : forall (n : nat), n >= n.
Proof.
  crush.
Qed.

(*
Lemma antisym : forall (x y : nat), x <= y -> y <= x -> x = y.
Proof.
  Print Hint *.
  intro x. induction x.
  intro y. induction y.
  crush.
  intro. intro. assert False by exact (zgtxfalse H0). contradiction.
  induction y. intro. assert False by exact (zgtxfalse H). contradiction.
  intro H0. induction H0. intro H1. inversion H1. trivial.

*)